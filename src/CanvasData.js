const CanvasData = {
    canvas : null,
    canvasCtx : null,
    canvas2 : null,
    canvas2Ctx : null,
    previewCanvas : null,
    previewCtx : null,
    backgroundColor : "#dcdcdc"
}


export default CanvasData