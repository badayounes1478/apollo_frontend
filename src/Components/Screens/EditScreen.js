import React, { useState, useEffect } from 'react'
import '../ScreensCss/EditScreen.css'
import Template from '../../assets/layout.svg'
import Upload from '../../assets/upload.svg'
import Picture from '../../assets/picture.svg'
import Design from '../../assets/3d-design.svg'
import Font from '../../assets/font.svg'
import App from '../../assets/app.svg'
import Graph from '../../assets/graph.svg'
import Document from '../../assets/document.svg'
import More from '../../assets/more.svg'
import Axios from "axios"
import CanvasData from '../../CanvasData'
import Modal from '../Reusable Components/Modal'
import {
    moveTheAnimationLeftToRightText,
    moveTheAnimationLeftToRightSvg,
    moveTheAnimationLeftToRightImage,
    moveTheAnimationLeftToRightVideo
} from '../../AnimationFunctiones/LeftToRightAnimation'

import {
    moveTheAnimationRighToLeftSvg,
    moveTheAnimationRightToLeftImage,
    moveTheAnimationRightToLeftVideo,
    moveTheAnimationRightToLeftText
} from '../../AnimationFunctiones/RightToLeftAnimation'

import {
    moveTheAnimationTopToBottomText,
    moveTheAnimationTopToBottomVideo,
    moveTheAnimationTopToBottomImage,
    moveTheAnimationTopToBottomSvg
} from '../../AnimationFunctiones/TopToBottomAnimation'

import {
    moveTheAnimationBottomToTopImage,
    moveTheAnimationBottomToTopSvg,
    moveTheAnimationBottomToTopText,
    moveTheAnimationBottomToTopVideo
} from '../../AnimationFunctiones/BottomToTopAnimation'

import {
    zoomText,
    zoomSvg
} from '../../AnimationFunctiones/ZoomAnimation'

import facebook from '../../assets/app.svg'

const EditScreen = (props) => {
    const colorsHash = {};
    var offsetX = 0
    var offsetY = 0
    var isDragging = false;
    var selectedShape;
    const [active, setactive] = useState(["active", "inactive", "inactive", "inactive", "inactive", "inactive", "inactive", "inactive", "inactive"])
    const [images, setimages] = useState([])
    const [videos, setvideos] = useState([])
    const [arrayOfElements, setarrayOfElements] = useState([])
    const [opacity, setopacity] = useState(1)
    const [toggleModal, settoggleModal] = useState(false)
    const [arrayOfSeen, setarrayOfSeen] = useState([])
    const [seen, setseen] = useState(1)
    const [activeObject, setactiveObject] = useState(null)
    const [currentSeen, setcurrentSeen] = useState(null)
    const [copyElement, setcopyElement] = useState(null)
    const [duration, setduration] = useState(500)
    const [canvasDimension, setcanvasDimension] = useState(null)
    const [audio, setaudio] = useState([
        { url: require('../../assets/music2.mp3'), fileName: 'music2.mp3' },
        { url: require('../../assets/music3.mp3'), fileName: 'music3.mp3' },
        { url: require('../../assets/music4.mp3'), fileName: "music4.mp3" },
        { url: require('../../assets/music5.mp3'), fileName: "music5.mp3" }
    ])
    const [downloadVideos, setdownloadVideos] = useState([])
    let id2

    const getTheImagesAndVideos = async () => {
        const vid = await Axios.get('https://pixabay.com/api/videos/?key=17413650-b71223c8282f00889b36892d3')
        setvideos(vid.data.hits)
        const images = await Axios.get('https://pixabay.com/api/?key=17413650-b71223c8282f00889b36892d3')
        setimages(images.data.hits)
    }

    const chnageTheActiveElement = (id) => {
        let changeArrayElement = active
        let n = changeArrayElement.length

        if (id === 2) {
            if (videos.length === 0 && images.length === 0) {
                getTheImagesAndVideos()
            }
        } else if (id === 7) {
            Axios.get('https://apollo.yogved.in/apollo/video/all/videos').then(res => {
                let downloadedLinkAdded = res.data.map(data => {
                    data.download = `https://apollo.yogved.in/apollo/video/download/${data._id}`
                    return data
                })
                setdownloadVideos(downloadedLinkAdded)
            }).catch(err => {
                console.log(err.response)
            })
        }

        for (let index = 0; index < n; index++) {
            if (id === index) {
                changeArrayElement[index] = "active"
            } else {
                changeArrayElement[index] = "inactive"
            }
        }
        setactive([...changeArrayElement])
    }

    /*********************  add The Elements to the canvas section ******************/

    useEffect(() => {
        setcanvasDimension({ width: props.location.state.width, height: props.location.state.height })
        setarrayOfElements([{ id: new Date().getTime(), x: 100, y: 100, width: 150, height: 100, color: "orange", type: 'rectangle', animation: null, opacity: 1.0 }, { id: new Date().getTime(), x: 50, y: 40, type: 'text', color: "red", size: 30, animation: null, opacity: 1.0 }, { id: new Date().getTime(), x: 50, y: 40, type: 'svg', color: "red", height: 100, width: 100, animation: null, opacity: 1.0 }])
        CanvasData.canvas = document.getElementById('canvas');
        CanvasData.canvasCtx = CanvasData.canvas.getContext('2d');

        CanvasData.canvasCtx.fillStyle = CanvasData.backgroundColor;
        CanvasData.canvasCtx.fillRect(0, 0, CanvasData.canvas.width, CanvasData.canvas.height);

        CanvasData.canvas2 = document.getElementById('canvas1');
        CanvasData.canvas2Ctx = CanvasData.canvas2.getContext('2d');

        arrayOfElements.forEach(data => {
            if (data.type === "rectangle") {
                drawRectangleOnCanvas(data)
            } else if (data.type === "image") {
                drawImageOnCanvas(data)
            } else if (data.type === "video") {
                drawVideoOnCanvas(data)
            } else if (data.type === "text") {
                drawTextOnCanvas(data)
            } else if (data.type === "svg") {
                drawSvgOnCanvas(data)
            }
        });

        CanvasData.canvas.onmousedown = handleMouseDown;
        CanvasData.canvas.onmousemove = handleMouseMove;
        CanvasData.canvas.onmouseup = handleMouseUp;

    }, [])

    useEffect(() => {
        CanvasData.canvasCtx.clearRect(0, 0, CanvasData.canvas.width, CanvasData.canvas.height);
        CanvasData.canvas2Ctx.clearRect(0, 0, CanvasData.canvas.width, CanvasData.canvas.height);

        CanvasData.canvasCtx.fillStyle = CanvasData.backgroundColor;;
        CanvasData.canvasCtx.fillRect(0, 0, CanvasData.canvas.width, CanvasData.canvas.height);

        CanvasData.canvas.onmousedown = handleMouseDown;
        CanvasData.canvas.onmousemove = handleMouseMove;
        CanvasData.canvas.onmouseup = handleMouseUp;

        arrayOfElements.forEach(data => {
            if (data.type === "rectangle") {
                drawRectangleOnCanvas(data)
            } else if (data.type === "image") {
                drawImageOnCanvas(data)
            } else if (data.type === "video") {
                drawVideoOnCanvas(data)
            } else if (data.type === "text") {
                drawTextOnCanvas(data)
            } else if (data.type === "svg") {
                drawSvgOnCanvas(data)
            }
        });
    }, [arrayOfElements])

    const drawAll = () => {

        CanvasData.canvasCtx.clearRect(0, 0, CanvasData.canvas.width, CanvasData.canvas.height);
        CanvasData.canvas2Ctx.clearRect(0, 0, CanvasData.canvas.width, CanvasData.canvas.height);

        CanvasData.canvasCtx.fillStyle = CanvasData.backgroundColor;
        CanvasData.canvasCtx.fillRect(0, 0, CanvasData.canvas.width, CanvasData.canvas.height);

        arrayOfElements.forEach(data => {
            if (data.type === "rectangle") {
                drawRectangleOnCanvas(data)
            } else if (data.type === "image") {
                drawImageOnCanvas(data)
            } else if (data.type === "video") {
                drawVideoOnCanvas(data)
            } else if (data.type === "text") {
                drawTextOnCanvas(data)
            } else if (data.type === "svg") {
                drawSvgOnCanvas(data)
            }
        });
    }


    useEffect(() => {

    }, [activeObject])


    /**************Mouse Events************/

    const handleMouseDown = (e) => {
        e.preventDefault();
        e.stopPropagation();
        const mousePos = {
            x: e.clientX - CanvasData.canvas.offsetLeft,
            y: e.clientY - CanvasData.canvas.offsetTop
        };
        const pixel = CanvasData.canvas2Ctx.getImageData(mousePos.x, mousePos.y, 1, 1).data;
        const color = `rgb(${pixel[0]},${pixel[1]},${pixel[2]})`;
        const shape = colorsHash[color];
        if (shape) {
            drawAll()
            if (shape.type === "image" || shape.type === "video" || shape.type === "svg") {
                CanvasData.canvasCtx.beginPath();
                CanvasData.canvasCtx.rect(shape.x, shape.y, shape.width / 3, shape.height / 3);
                CanvasData.canvasCtx.lineWidth = 2;
                CanvasData.canvasCtx.strokeStyle = "blue";
                CanvasData.canvasCtx.stroke();
            } else if (shape.type === "text") {
                let width = Math.round(CanvasData.canvasCtx.measureText("hello world").width) + 12
                CanvasData.canvasCtx.beginPath();
                CanvasData.canvasCtx.rect(shape.x, shape.y, width, shape.size);
                CanvasData.canvasCtx.lineWidth = 2;
                CanvasData.canvasCtx.strokeStyle = "blue";
                CanvasData.canvasCtx.stroke();
            } else {
                CanvasData.canvasCtx.beginPath();
                CanvasData.canvasCtx.rect(shape.x, shape.y, shape.width, shape.height);
                CanvasData.canvasCtx.lineWidth = 2;
                CanvasData.canvasCtx.strokeStyle = "blue";
                CanvasData.canvasCtx.stroke();
            }
            setactiveObject(() => shape)
            selectedShape = shape
            offsetX = e.clientX - selectedShape.x
            offsetY = e.clientY - selectedShape.y;
            isDragging = true
        } else {
            drawAll();
        }
    }

    const handleMouseUp = (e) => {
        if (!isDragging) { return; }
        e.preventDefault();
        e.stopPropagation();
        isDragging = false;
        selectedShape = null
    }

    const handleMouseMove = (e) => {
        if (selectedShape === null) return
        if (isDragging === true) {
            e.preventDefault();
            e.stopPropagation();
            selectedShape.x = e.clientX - offsetX;
            selectedShape.y = e.clientY - offsetY
            let tempArray = arrayOfElements
            for (let i = 0; i < tempArray.length; i++) {
                if (selectedShape.colorKey === tempArray[i].colorKey) {
                    tempArray[i] = selectedShape
                }
            }
            setarrayOfElements(() => tempArray)
            drawAll();
            if (selectedShape.type === "image" || selectedShape.type === "video" || selectedShape.type === "svg") {
                CanvasData.canvasCtx.beginPath();
                CanvasData.canvasCtx.rect(selectedShape.x, selectedShape.y, selectedShape.width / 3, selectedShape.height / 3);
                CanvasData.canvasCtx.stroke();
            } else if (selectedShape.type === "text") {
                let width = Math.round(CanvasData.canvasCtx.measureText("hello world").width) + 12
                CanvasData.canvasCtx.beginPath();
                CanvasData.canvasCtx.rect(selectedShape.x, selectedShape.y, width, selectedShape.size);
                CanvasData.canvasCtx.stroke();
            } else {
                CanvasData.canvasCtx.beginPath();
                CanvasData.canvasCtx.rect(selectedShape.x, selectedShape.y, selectedShape.width, selectedShape.height);
                CanvasData.canvasCtx.stroke();
            }
        }
    }

    /**************Mouse Events End ************/

    /************** Draw Method Start ************/

    function getRandomColor() {
        const r = Math.round(Math.random() * 255);
        const g = Math.round(Math.random() * 255);
        const b = Math.round(Math.random() * 255);
        return `rgb(${r},${g},${b})`;
    }

    const drawImageOnCanvas = (data) => {
        var img = document.getElementById(data.elementId);
        arrayOfElements.forEach(circle => {
            while (true) {
                const colorKey = getRandomColor();
                if (!colorsHash[colorKey]) {
                    circle.colorKey = colorKey;
                    colorsHash[colorKey] = circle;
                    return circle.colorKey
                }
            }
        });

        CanvasData.canvasCtx.globalAlpha = data.opacity
        CanvasData.canvasCtx.drawImage(img, data.x, data.y, data.width / 3, data.height / 3);
        CanvasData.canvasCtx.globalAlpha = 1.0

        addReact(data.height / 3, data.width / 3)

        function addReact(height, width) {
            CanvasData.canvas2Ctx.beginPath();
            CanvasData.canvas2Ctx.rect(data.x, data.y, width, height);
            CanvasData.canvas2Ctx.fillStyle = data.colorKey;
            CanvasData.canvas2Ctx.fill();
        }
    }

    const drawRectangleOnCanvas = (rectangle) => {
        arrayOfElements.forEach(circle => {
            while (true) {
                const colorKey = getRandomColor();
                if (!colorsHash[colorKey]) {
                    circle.colorKey = colorKey;
                    colorsHash[colorKey] = circle;
                    return circle.colorKey
                }
            }
        });

        CanvasData.canvasCtx.beginPath();
        CanvasData.canvasCtx.globalAlpha = rectangle.opacity
        CanvasData.canvasCtx.rect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        CanvasData.canvasCtx.fillStyle = rectangle.color;
        CanvasData.canvasCtx.fill();
        CanvasData.canvasCtx.globalAlpha = 1.0


        CanvasData.canvas2Ctx.beginPath();
        CanvasData.canvas2Ctx.rect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        CanvasData.canvas2Ctx.fillStyle = rectangle.colorKey;
        CanvasData.canvas2Ctx.fill();
    }

    const drawVideoOnCanvas = (data) => {
        var video = document.getElementById(data.elementId);
        arrayOfElements.forEach(circle => {
            while (true) {
                const colorKey = getRandomColor();
                if (!colorsHash[colorKey]) {
                    circle.colorKey = colorKey;
                    colorsHash[colorKey] = circle;
                    return circle.colorKey
                }
            }
        });

        CanvasData.canvasCtx.globalAlpha = data.opacity
        CanvasData.canvasCtx.drawImage(video, data.x, data.y, data.width / 3, data.height / 3);
        CanvasData.canvasCtx.globalAlpha = 1.0

        addReact(data.height / 3, data.width / 3)

        function addReact(height, width) {
            CanvasData.canvas2Ctx.beginPath();
            CanvasData.canvas2Ctx.rect(data.x, data.y, width, height);
            CanvasData.canvas2Ctx.fillStyle = data.colorKey;
            CanvasData.canvas2Ctx.fill();
        }
    }


    const drawTextOnCanvas = (data) => {
        arrayOfElements.forEach(circle => {
            while (true) {
                const colorKey = getRandomColor();
                if (!colorsHash[colorKey]) {
                    circle.colorKey = colorKey;
                    colorsHash[colorKey] = circle;
                    return circle.colorKey
                }
            }
        });

        let txt = "Hello World"
        CanvasData.canvasCtx.globalAlpha = data.opacity
        CanvasData.canvasCtx.textBaseline = "top";
        CanvasData.canvasCtx.font = data.size + "px Arial";
        CanvasData.canvasCtx.fillStyle = data.color;
        CanvasData.canvasCtx.fillText("Hello World", data.x, data.y);
        CanvasData.canvasCtx.globalAlpha = 1.0



        let width = Math.round(CanvasData.canvasCtx.measureText(txt).width)
        let height = data.size

        addReact(width, height)
        function addReact(width, height) {
            CanvasData.canvas2Ctx.beginPath();
            CanvasData.canvas2Ctx.rect(data.x, data.y, width, height);
            CanvasData.canvas2Ctx.fillStyle = data.colorKey;
            CanvasData.canvas2Ctx.fill();
        }
    }

    const drawSvgOnCanvas = (data) => {
        var img = document.getElementById("svg1");
        arrayOfElements.forEach(circle => {
            while (true) {
                const colorKey = getRandomColor();
                if (!colorsHash[colorKey]) {
                    circle.colorKey = colorKey;
                    colorsHash[colorKey] = circle;
                    return circle.colorKey
                }
            }
        });

        changeIconColor(img, data)
        addReact(data.height / 3, data.width / 3)

        function addReact(height, width) {
            CanvasData.canvas2Ctx.beginPath();
            CanvasData.canvas2Ctx.rect(data.x, data.y, width, height);
            CanvasData.canvas2Ctx.fillStyle = data.colorKey;
            CanvasData.canvas2Ctx.fill();
        }
    }

    var changeIconColor = (img, data) => {
        let canvas = document.getElementById("mayur")
        canvas.width = data.width / 3
        canvas.height = data.height / 3

        let ctx = canvas.getContext("2d")
        ctx.clearRect(0, 0, data.width / 3, data.height / 3);
        ctx.drawImage(img, 0, 0, data.width / 3, data.height / 3);
        ctx.globalCompositeOperation = "source-atop";
        ctx.globalAlpha = 1.0;
        ctx.fillStyle = "blue";
        ctx.fillRect(0, 0, data.width / 3, data.height / 3);
        // reset
        ctx.globalCompositeOperation = "source-over";
        ctx.globalAlpha = 1.0;

        ctx.globalCompositeOperation = "destination-in";
        ctx.drawImage(img, 0, 0, data.width / 3, data.height / 3);
        ctx.globalCompositeOperation = "source-over";
        let data1 = canvas.toDataURL("img/png", 1.0)
        var img1 = new Image();
        img1.src = data1;
        img1.onload = function () {
            CanvasData.canvasCtx.globalAlpha = data.opacity
            CanvasData.canvasCtx.drawImage(img1, data.x, data.y);
            CanvasData.canvasCtx.globalAlpha = 1.0
        }
    }

    /************** Draw Method End ************/


    const addImage = (id, width, height, src) => {
        let image = { id: new Date().getTime(), elementId: id, x: 10, y: 10, width: width, height: height, type: "image", src: src, animation: null, opacity: 1.0 }
        setarrayOfElements(prevArrayOfElements => ([...prevArrayOfElements, image]));
    }

    const captureTheImage = (id, width, height, src) => {
        let video = { id: new Date().getTime(), elementId: id, x: 10, y: 10, width: width, height: height, type: "video", src: src, animation: null, opacity: 1.0 }
        setarrayOfElements(prevArrayOfElements => ([...prevArrayOfElements, video]));
    }

    /**********************add The Elements to the canvas section *****************/

    const toggleModalFunction = () => {
        settoggleModal(!toggleModal)
        if (toggleModal === true) {
            setopacity(1)
        } else {
            setopacity(0.1)
            setTimeout(() => {
                callPreview()
            }, 1000);
        }
    }

    const Preview = (data1, data2) => {
        CanvasData.previewCtx.clearRect(0, 0, CanvasData.canvas.width, CanvasData.canvas.height);
        CanvasData.previewCtx.fillStyle = data2.backgroundColor;
        CanvasData.previewCtx.fillRect(0, 0, CanvasData.previewCanvas.width, CanvasData.previewCanvas.height);

        data1.map(data => {
            if (data.type === "rectangle") {
                previewDrawRectangleOnCanvas(data)
            } else if (data.type === "image") {
                previewDrawImageOnCanvas(data)
            } else if (data.type === "video") {
                previewDrawVideoOnCanvas(data)
            } else if (data.type === "text") {
                previewDrawTextOnCanvas(data)
            } else if (data.type === "svg") {
                previewSvgOnCanvas(data)
            } else if (data.type === "audio") {
                previewAudioOnCanvas(data, data2.duration)
            }
        })
        id2 = requestAnimationFrame(() => Preview(data1, data2))
    }


    const callPreview = () => {
        cancelAnimationFrame(id2);
        CanvasData.previewCanvas = document.getElementById("canvas2");
        CanvasData.previewCtx = CanvasData.previewCanvas.getContext('2d');
        let time = 0
        arrayOfSeen.map((data, i) => {
            if (i === 0) {
                id2 = requestAnimationFrame(() => Preview(data.data, data));
                time = time + parseInt(data.duration)
            } else {
                setTimeout(() => {
                    cancelAnimationFrame(id2)
                    id2 = requestAnimationFrame(() => Preview(data.data, data))
                }, time);
                time = time + parseInt(data.duration)
            }
        })

        setTimeout(() => {
            cancelAnimationFrame(id2)
            //set the animation from start again
            arrayOfSeen.map(data1 => {
                data1.data.map(data => {
                    if (data.type === "audio") {
                        let audio = document.getElementById(data.elementId)
                        audio.pause()
                        audio.currentTime = 0
                        data.flag = 0
                    } else if (data.type === "video") {
                        let video = document.getElementById(data.elementId)
                        video.pause()
                        audio.currentTime = 0
                    }
                    if (data.animation) {
                        if (data.animation.type === "right-to-left") {
                            data.animation = { play: true, x: 640, y: 0, type: "right-to-left", speed: 1, delay: 2000 }
                        } else if (data.animation.type === "left-to-right") {
                            data.animation = { play: true, x: 0, y: 0, type: "left-to-right", speed: 1, delay: 2000 }
                        } else if (data.animation.type === "top-to-bottom") {
                            data.animation = { play: true, x: 0, y: 0, type: "top-to-bottom", speed: 1, delay: 2000 }
                        } else if (data.animation.type === "bottom-to-top") {
                            data.animation = { play: true, x: 0, y: 360, type: "bottom-to-top", speed: 1, delay: 2000 }
                        }
                    }
                })
            })
        }, time);
    }

    const previewAudioOnCanvas = (audio) => {
        let audioToPlay = document.getElementById(audio.elementId)
        if (audio.flag === 0) {
            audio.flag = 1
            if (!isNaN(audio.startTime)) {
                audioToPlay.currentTime = audio.startTime
            }
            if (!isNaN(audio.endTime)) {
                let endTime = audio.endTime * 1000
                setTimeout(() => {
                    audioToPlay.pause()
                }, endTime);
            }
            audioToPlay.play()
        } else if (audio.flag === 0) {
            audio.flag = 1
            audioToPlay.play()
        }
    }

    const previewDrawTextOnCanvas = (text) => {
        if (text.animation !== null) {
            if (text.animation.type === "left-to-right") {
                moveTheAnimationLeftToRightText(text)
            } else if (text.animation.type === "right-to-left") {
                moveTheAnimationRightToLeftText(text)
            } else if (text.animation.type === "top-to-bottom") {
                moveTheAnimationTopToBottomText(text)
            } else if (text.animation.type === "bottom-to-top") {
                moveTheAnimationBottomToTopText(text)
            } else if (text.animation.type === "zoom") {
                zoomText(text)
            }
        } else {
            CanvasData.previewCtx.globalAlpha = text.opacity
            CanvasData.previewCtx.font = text.size + "px Arial";
            CanvasData.previewCtx.fillStyle = text.color;
            CanvasData.previewCtx.fillText("Hello World", text.x, text.y);
            CanvasData.previewCtx.globalAlpha = 1.0
        }
    }

    const previewDrawVideoOnCanvas = (data) => {
        if (data.animation !== null) {
            if (data.animation.type === "left-to-right") {
                moveTheAnimationLeftToRightVideo(data)
            } else if (data.animation.type === "right-to-left") {
                moveTheAnimationRightToLeftVideo(data)
            } else if (data.animation.type === "top-to-bottom") {
                moveTheAnimationTopToBottomVideo(data)
            } else if (data.animation.type === "bottom-to-top") {
                moveTheAnimationBottomToTopVideo(data)
            }
        } else {
            CanvasData.previewCtx.globalAlpha = data.opacity
            var video = document.getElementById(data.elementId);
            CanvasData.previewCtx.drawImage(video, data.x, data.y, data.width / 3, data.height / 3);
            CanvasData.previewCtx.globalAlpha = 1.0
            video.play()
        }
    }

    const previewDrawImageOnCanvas = (image) => {
        if (image.animation !== null) {
            if (image.animation.type === "left-to-right") {
                moveTheAnimationLeftToRightImage(image)
            } else if (image.animation.type === "right-to-left") {
                moveTheAnimationRightToLeftImage(image)
            } else if (image.animation.type === "top-to-bottom") {
                moveTheAnimationTopToBottomImage(image)
            } else if (image.animation.type === "bottom-to-top") {
                moveTheAnimationBottomToTopImage(image)
            }
        } else {
            CanvasData.previewCtx.globalAlpha = image.opacity
            var img = document.getElementById(image.elementId);
            CanvasData.previewCtx.drawImage(img, image.x, image.y, image.width / 3, image.height / 3);
            CanvasData.previewCtx.globalAlpha = 1.0
        }
    }

    const previewDrawRectangleOnCanvas = (rectangle) => {
        if (rectangle.animation !== null) {
            if (rectangle.animation.type === "left-to-right") {
                moveTheAnimationLeftToRightSvg(rectangle)
            } else if (rectangle.animation.type === "right-to-left") {
                moveTheAnimationRighToLeftSvg(rectangle)
            } else if (rectangle.animation.type === "top-to-bottom") {
                moveTheAnimationTopToBottomSvg(rectangle)
            } else if (rectangle.animation.type === "bottom-to-top") {
                moveTheAnimationBottomToTopSvg(rectangle)
            } else if (rectangle.animation.type === "zoom") {
                zoomSvg(rectangle)
            }
        } else {
            CanvasData.previewCtx.globalAlpha = rectangle.opacity
            CanvasData.previewCtx.beginPath();
            CanvasData.previewCtx.fillStyle = rectangle.color;
            CanvasData.previewCtx.rect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
            CanvasData.previewCtx.fill();
            CanvasData.previewCtx.globalAlpha = 1.0
        }
    }

    const previewSvgOnCanvas = (data) => {
        var img = document.getElementById("svg1");
        CanvasData.previewCtx.globalAlpha = data.opacity
        CanvasData.previewCtx.drawImage(img, data.x, data.y, data.width / 3, data.height / 3);
        CanvasData.previewCtx.globalAlpha = 1.0
    }

    const showSeen = (index) => {
        setcurrentSeen(index.seenNo)
        setarrayOfElements(index.data)
    }

    const nextSeen = () => {
        let seenObject = {
            backgroundColor: CanvasData.backgroundColor,
            seenNo: seen,
            duration: duration,
            data: arrayOfElements,
            image: CanvasData.canvas.toDataURL("image/jpeg", 0.80)
        }
        setseen(seen => seen + 1)
        setarrayOfSeen(previousData => [...previousData, seenObject])
        setarrayOfElements([])
    }

    const saveToExistingSeens = () => {
        if (!currentSeen) return
        let seenObject = {
            backgroundColor: CanvasData.backgroundColor,
            seenNo: seen,
            duration: duration,
            data: arrayOfElements,
            image: CanvasData.canvas.toDataURL("image/jpeg", 0.80)
        }

        let data = arrayOfSeen.map((seen) => {
            if (seen.seenNo === currentSeen) {
                return seenObject
            } else {
                return seen
            }
        })

        setarrayOfSeen([...data])
        setarrayOfElements([])
        setcurrentSeen(null)
    }

    const sendImagesToBackend = (e) => {
        e.preventDefault()
        Axios.post('https://apollo.yogved.in/apollo/generate/video/' + props.match.params.id, {
            arrayOfSeen: arrayOfSeen,
            canvasDimension: canvasDimension
        }).then(res => {
            console.log(res.data)
        })
    }

    const sendBack = () => {
        if (!activeObject) return
        let index = arrayOfElements.findIndex(x => x.colorKey === activeObject.colorKey);
        if (index === 0) return
        let objectToSwap = arrayOfElements.filter((data, i) => {
            if (i === index - 1) {
                return data
            }
        })
        let filterArray = arrayOfElements.map((data, i) => {
            if (i === index - 1) {
                return activeObject
            }
            if (i === index) {
                return objectToSwap[0]
            }
            return data
        })
        setarrayOfElements(filterArray)
    }

    const sendFront = () => {
        if (!activeObject) return
        let index = arrayOfElements.findIndex(x => x.colorKey === activeObject.colorKey);
        if (index === arrayOfElements.length - 1) return
        let objectToSwap = arrayOfElements.filter((data, i) => {
            if (i === index + 1) {
                return data
            }
        })
        let filterArray = arrayOfElements.map((data, i) => {
            if (i === index + 1) {
                return activeObject
            }
            if (i === index) {
                return objectToSwap[0]
            }
            return data
        })
        setarrayOfElements(filterArray)
    }


    const sendToBack = () => {
        if (!activeObject) return
        let addToLastInCanvas = arrayOfElements.filter((data) => {
            if (activeObject.colorKey === data.colorKey) {
                return data
            }
        })

        let removeElementFromArrayWhichIsActive = arrayOfElements.filter((data) => {
            if (activeObject.colorKey !== data.colorKey) {
                return data
            }
        })

        setarrayOfElements([addToLastInCanvas[0], ...removeElementFromArrayWhichIsActive])
    }

    const sendToFront = () => {
        if (!activeObject) return
        let addToFirstInCanvas = arrayOfElements.filter((data) => {
            if (activeObject.colorKey === data.colorKey) {
                return data
            }
        })

        let removeElementFromArrayWhichIsActive = arrayOfElements.filter((data) => {
            if (activeObject.colorKey !== data.colorKey) {
                return data
            }
        })
        setarrayOfElements([...removeElementFromArrayWhichIsActive, addToFirstInCanvas[0]])
    }

    const leftToRightAnimation = () => {
        let addedAnimationData = arrayOfElements
        addedAnimationData.forEach(data => {
            if (data.colorKey === activeObject.colorKey) {
                return data.animation = { play: true, x: 0, y: 0, type: "left-to-right", speed: 1, delay: 2000 }
            }
        })
        setarrayOfElements(addedAnimationData)
    }

    const RightToLeftAnimation = () => {
        let addedAnimationData = arrayOfElements
        addedAnimationData.forEach(data => {
            if (data.colorKey === activeObject.colorKey) {
                return data.animation = { play: true, x: 640, y: 0, type: "right-to-left", speed: 1, delay: 2000 }
            }
        })
        setarrayOfElements(addedAnimationData)
    }

    const TopToBottomAnimation = () => {
        let addedAnimationData = arrayOfElements
        addedAnimationData.forEach(data => {
            if (data.colorKey === activeObject.colorKey) {
                return data.animation = { play: true, x: 0, y: 0, type: "top-to-bottom", speed: 1, delay: 2000 }
            }
        })
        setarrayOfElements(addedAnimationData)
    }

    const BottomToTopAnimation = () => {
        let addedAnimationData = arrayOfElements
        addedAnimationData.forEach(data => {
            if (data.colorKey === activeObject.colorKey) {
                return data.animation = { play: true, x: 0, y: 360, type: "bottom-to-top", speed: 1, delay: 2000 }
            }
        })
        setarrayOfElements(addedAnimationData)
    }

    const ZoomInOutAnimation = () => {
        let addedAnimationData = arrayOfElements
        addedAnimationData.forEach(data => {
            if (data.colorKey === activeObject.colorKey) {
                if (activeObject.type === "text") {
                    return data.animation = { size: activeObject.size, type: "zoom", speed: 1, zoomTo: activeObject.size + 10, flag: 1, delay: 2000 }
                } else {
                    return data.animation = { width: activeObject.width, type: "zoom", height: activeObject.height, speed: 0.2, zoomWidth: activeObject.width + 20, zoomHeight: activeObject.height + 20, flag: 1, delay: 2000 }
                }
            }
        })
        setarrayOfElements(addedAnimationData)
    }

    const saveTemplate = () => {
        Axios.post('http://103.12.211.43:4000/template/', {
            object: JSON.stringify(arrayOfSeen)
        }).then(data => {
            console.log(data)
        })
    }

    const duplicateTheElement = () => {
        if (!activeObject) return
        let data
        if (activeObject.type === "image") {
            data = {
                id: new Date().getTime(),
                x: activeObject.x + 10,
                y: activeObject.y + 10,
                width: activeObject.width,
                height: activeObject.height,
                elementId: activeObject.elementId,
                type: "image",
                src: activeObject.src,
                animation: activeObject.animation,
            }
        } else if (activeObject.type === "video") {
            data = {
                id: new Date().getTime(),
                elementId: activeObject.elementId,
                x: activeObject.x + 10,
                y: activeObject.y + 10,
                width: activeObject.width,
                height: activeObject.height,
                type: "video",
                src: activeObject.src,
                animation: activeObject.animation
            }
        } else if (activeObject.type === "text") {
            data = {
                id: new Date().getTime(),
                x: activeObject.x + 10,
                y: activeObject.y + 10,
                type: 'text',
                size: activeObject.size,
                animation: activeObject.animation,
                color: activeObject.color
            }
        } else if (activeObject.type === "rectangle") {
            data = {
                id: new Date().getTime(),
                x: activeObject.x + 10,
                y: activeObject.y + 10,
                width: activeObject.width,
                height: activeObject.height,
                color: activeObject.color,
                type: 'rectangle',
                animation: activeObject.animation
            }
        }
        setarrayOfElements(prev => [...prev, data])
    }


    const copyTheElement = () => {
        setcopyElement(activeObject)
    }

    const pastTheElement = () => {
        if (!copyElement) return
        let data
        if (copyElement.type === "image") {
            data = {
                id: new Date().getTime(),
                x: copyElement.x + 10,
                y: copyElement.y + 10,
                width: copyElement.width,
                height: copyElement.height,
                elementId: copyElement.elementId,
                type: "image",
                src: copyElement.src,
                animation: copyElement.animation,
            }
        } else if (copyElement.type === "video") {
            data = {
                id: new Date().getTime(),
                elementId: copyElement.elementId,
                x: copyElement.x + 10,
                y: copyElement.y + 10,
                width: copyElement.width,
                height: copyElement.height,
                type: "video",
                src: copyElement.src,
                animation: copyElement.animation
            }
        } else if (copyElement.type === "text") {
            data = {
                id: new Date().getTime(),
                x: copyElement.x + 10,
                y: copyElement.y + 10,
                type: 'text',
                size: copyElement.size,
                animation: copyElement.animation,
                color: copyElement.color
            }
        } else if (copyElement.type === "rectangle") {
            data = {
                id: new Date().getTime(),
                x: copyElement.x + 10,
                y: copyElement.y + 10,
                width: copyElement.width,
                height: copyElement.height,
                color: copyElement.color,
                type: 'rectangle',
                animation: copyElement.animation
            }
        }
        setarrayOfElements(prev => [...prev, data])
    }


    const deleteElement = () => {
        if (!activeObject) return
        let filterArray = arrayOfElements.filter(data => {
            if (data.colorKey !== activeObject.colorKey) {
                return data
            }
        })
        setarrayOfElements([...filterArray])
    }

    const deleteSeen = () => {
        if (!currentSeen) return
        let data = arrayOfSeen.filter(data => {
            if (data.seenNo !== currentSeen) {
                return data
            }
        })
        setarrayOfSeen([...data])
        setarrayOfElements([])
        setcurrentSeen(null)
    }



    const chaneBackgroundColor = (color) => {
        CanvasData.backgroundColor = color
        drawAll()
    }

    const setTheOpacity = (e) => {
        if (!activeObject) return
        let number = parseInt(e.target.value)
        let value
        if (number === 10) {
            value = 1.0
        } else {
            let convertToNumber = "0." + number
            value = parseFloat(convertToNumber)
        }
        let data1 = activeObject
        data1.opacity = value
        let filterArray = arrayOfElements.filter(data => {
            if (data.colorKey === activeObject.colorKey) {
                return data1
            } else {
                return data
            }
        })
        setarrayOfElements([...filterArray])
    }

    const changeTheSpeed = (e) => {
        if (!activeObject) return
        if (!activeObject.animation) return
        let data1 = activeObject
        data1.animation.speed = parseInt(e.target.value)
        let filterArray = arrayOfElements.filter(data => {
            if (data.colorKey === activeObject.colorKey) {
                return data1
            } else {
                return data
            }
        })
        setarrayOfElements([...filterArray])
    }

    const addAudio = (src, audioId, start, end, volume, fileName) => {
        let startTime = parseFloat(document.getElementById(start).value)
        let endTime = parseFloat(document.getElementById(end).value)
        let soundVolume = parseFloat(document.getElementById(volume).value)
        let duration = parseFloat(document.getElementById(audioId).duration)
        if (endTime > duration) {
            alert("end time is greater then video duration")
        }
        let audio = { id: new Date().getTime(), elementId: audioId, duration: duration, startTime: startTime, endTime: endTime, volume: soundVolume, src: src, type: "audio", flag: 0, fileName: fileName }
        setarrayOfElements(prevArrayOfElements => ([...prevArrayOfElements, audio]));
    }


    return (
        <div className="edit-container-screen">
            {
                toggleModal === true ? <Modal state={props.location.state} /> : null
            }
            <div className="edit-screen" style={{ opacity: opacity }} onClick={() => toggleModal === true ? toggleModalFunction() : null}  >
                <nav>
                    <div>
                        <button>Home</button>
                        <button>File</button>
                        <button>Resize</button>
                        <button>All Changes Saved</button>
                    </div>
                    <div>
                        <strong>Untitled Design</strong>
                    </div>
                    <div>
                        <button>Share</button>
                        <button>Download</button>
                        <button>Present</button>
                    </div>
                </nav>

                <section className="screen-container">
                    <div>
                        <div className="icons-container" style={{ backgroundColor: active[0] === "active" ? "#264653" : "#1B323B" }} onClick={() => chnageTheActiveElement(0)} >
                            <img id="template" src={Template} alt="templates" />
                            <label htmlFor="template">Template</label>
                        </div>
                        <div className="icons-container" style={{ backgroundColor: active[1] === "active" ? "#264653" : "#1B323B" }} onClick={() => chnageTheActiveElement(1)} >
                            <img id="upload" src={Upload} alt="upload" />
                            <label htmlFor="upload">Animation</label>
                        </div>
                        <div className="icons-container" style={{ backgroundColor: active[2] === "active" ? "#264653" : "#1B323B" }} onClick={() => chnageTheActiveElement(2)} >
                            <img id="picture" src={Picture} alt="picture1" />
                            <label htmlFor="picture">Photos</label>
                        </div>
                        <div className="icons-container" style={{ backgroundColor: active[3] === "active" ? "#264653" : "#1B323B" }} onClick={() => chnageTheActiveElement(3)} >
                            <img id="design" src={Design} alt="design" />
                            <label htmlFor="design">Layers</label>
                        </div>
                        <div className="icons-container" style={{ backgroundColor: active[4] === "active" ? "#264653" : "#1B323B" }} onClick={() => chnageTheActiveElement(4)} >
                            <img id="font" src={Font} alt="font" />
                            <label htmlFor="font">Text</label>
                        </div>
                        <div className="icons-container" style={{ backgroundColor: active[5] === "active" ? "#264653" : "#1B323B" }} onClick={() => chnageTheActiveElement(5)} >
                            <img id="app" src={App} alt="app" />
                            <label htmlFor="app">Background</label>
                        </div>
                        <div className="icons-container" style={{ backgroundColor: active[6] === "active" ? "#264653" : "#1B323B" }} onClick={() => chnageTheActiveElement(6)} >
                            <img id="graph" src={Graph} alt="graph" />
                            <label htmlFor="graph">Audio</label>
                        </div>
                        <div className="icons-container" style={{ backgroundColor: active[7] === "active" ? "#264653" : "#1B323B" }} onClick={() => chnageTheActiveElement(7)} >
                            <img id="document" src={Document} alt="document" />
                            <label htmlFor="document">Folder</label>
                        </div>
                        <div className="icons-container" style={{ backgroundColor: active[8] === "active" ? "#264653" : "#1B323B" }} onClick={() => chnageTheActiveElement(8)} >
                            <img id="more" src={More} alt="more" />
                            <label htmlFor="more">More</label>
                        </div>
                    </div>

                    {/* End of the Feature icon container */}
                    {/* Start of the Feature container */}

                    <div className="features">
                        <div className="videos" style={active[2] === "active" ? { display: '' } : { display: 'none' }} >
                            <div>
                                <h4>Videos</h4>
                                {
                                    videos.map(data => {
                                        return <video
                                            muted={true}
                                            onMouseOver={event => event.target.play()}
                                            onMouseOut={event => event.target.pause()}
                                            key={data.picture_id}
                                            id={data.picture_id.toString()}
                                            src={data.videos.tiny.url}
                                            controls={false}
                                            onClick={() => captureTheImage(data.picture_id.toString(), data.videos.tiny.width, data.videos.tiny.height, data.videos.tiny.url)}
                                            crossOrigin="anonymous" />
                                    })
                                }
                            </div>
                            <div>
                                <h4>Images</h4>
                                {
                                    images.map(data => {
                                        return <img src={data.webformatURL}
                                            key={data.id}
                                            id={data.id}
                                            alt="image1"
                                            crossOrigin="anonymous"
                                            onClick={() => addImage(data.id.toString(), data.webformatWidth, data.webformatHeight, data.webformatURL)}
                                        />
                                    })
                                }
                            </div>
                        </div>
                        <div className="audio" style={active[6] === "active" ? { display: '' } : { display: 'none' }} >
                            {
                                audio.map((data, i) => {
                                    return <div style={{ display: 'flex', flexDirection: 'column', marginTop: 10, width: '100%' }}> <audio id={i + "audio"} controls>
                                        <source src={data.url} type="audio/mp3" />
                                    </audio>
                                        <input id={i + "start"} style={{ width: '80%', height: 10, alignSelf: 'center', padding: 5, marginTop: 5, borderRadius: 10 }} type="number" placeholder="start time"></input>
                                        <input id={i + "end"} style={{ width: '80%', height: 10, alignSelf: 'center', padding: 5, marginTop: 5, borderRadius: 10 }} type="number" placeholder="end time"></input>
                                        <input id={i + "volume"} style={{ width: '80%', height: 10, alignSelf: 'center', padding: 5, marginTop: 5, borderRadius: 10 }} type="number" min="1" max="10" placeholder="volume"></input>
                                        <button style={{ margin: 10, borderRadius: 10 }} onClick={() => addAudio(data.url, i + "audio", i + "start", i + "end", i + "volume", data.fileName)}>Add</button>
                                    </div>
                                })
                            }
                        </div>
                        {
                            active[1] === "active" ?
                                <div className="animation">
                                    <h4>Animation</h4>
                                    <button onClick={leftToRightAnimation}>left to right</button>
                                    <button onClick={RightToLeftAnimation}>right to left</button>
                                    <button onClick={TopToBottomAnimation}>top to bottom</button>
                                    <button onClick={BottomToTopAnimation}>bottom to top</button>
                                    <button onClick={ZoomInOutAnimation}>Zoom in Out</button>
                                </div>
                                : active[3] === "active" ? <div className="layer-container">
                                    {
                                        arrayOfElements.slice(0).reverse().map(data => {
                                            return <div className="layer-button">{data.type}</div>
                                        })
                                    }
                                </div> : active[5] === "active" ? <div className="color-container">
                                    <div className="color-subcontainer">
                                        <div className="color" style={{ backgroundColor: '#fe4a49' }} onClick={() => chaneBackgroundColor('#fe4a49')}></div>
                                        <div className="color" style={{ backgroundColor: '#2ab7ca' }} onClick={() => chaneBackgroundColor('#2ab7ca')}></div>
                                        <div className="color" style={{ backgroundColor: '#fed766' }} onClick={() => chaneBackgroundColor('#fed766')}></div>
                                        <div className="color" style={{ backgroundColor: '#e6e6ea' }} onClick={() => chaneBackgroundColor('#e6e6ea')}></div>
                                        <div className="color" style={{ backgroundColor: '#f4f4f8' }} onClick={() => chaneBackgroundColor('#f4f4f8')}></div>
                                        <div className="color" style={{ backgroundColor: '#eee3e7' }} onClick={() => chaneBackgroundColor('#eee3e7')}></div>
                                        <div className="color" style={{ backgroundColor: '#ead5dc' }} onClick={() => chaneBackgroundColor('#ead5dc')}></div>
                                        <div className="color" style={{ backgroundColor: '#eec9d2' }} onClick={() => chaneBackgroundColor('#eec9d2')}></div>
                                        <div className="color" style={{ backgroundColor: '#f4b6c2' }} onClick={() => chaneBackgroundColor('#f4b6c2')}></div>
                                        <div className="color" style={{ backgroundColor: '#f6abb6' }} onClick={() => chaneBackgroundColor('#f6abb6')}></div>
                                        <div className="color" style={{ backgroundColor: '#011f4b' }} onClick={() => chaneBackgroundColor('#011f4b')}></div>
                                        <div className="color" style={{ backgroundColor: '#03396c' }} onClick={() => chaneBackgroundColor('#03396c')}></div>
                                        <div className="color" style={{ backgroundColor: '#005b96' }} onClick={() => chaneBackgroundColor('#005b96')}></div>
                                        <div className="color" style={{ backgroundColor: '#6497b1' }} onClick={() => chaneBackgroundColor('#6497b1')}></div>
                                        <div className="color" style={{ backgroundColor: '#b3cde0' }} onClick={() => chaneBackgroundColor('#b3cde0')}></div>
                                    </div>
                                    <div className="color-subcontainer">
                                        <div className="color" style={{ backgroundColor: '#dec3c3' }} onClick={() => chaneBackgroundColor('#dec3c3')}></div>
                                        <div className="color" style={{ backgroundColor: '#e7d3d3' }} onClick={() => chaneBackgroundColor('#e7d3d3')}></div>
                                        <div className="color" style={{ backgroundColor: '#f0e4e4' }} onClick={() => chaneBackgroundColor('#f0e4e4')}></div>
                                        <div className="color" style={{ backgroundColor: '#f9f4f4' }} onClick={() => chaneBackgroundColor('#f9f4f4')}></div>
                                        <div className="color" style={{ backgroundColor: '#ffffff' }} onClick={() => chaneBackgroundColor('#ffffff')}></div>
                                        <div className="color" style={{ backgroundColor: '#4a4e4d' }} onClick={() => chaneBackgroundColor('#4a4e4d')}></div>
                                        <div className="color" style={{ backgroundColor: '#0e9aa7' }} onClick={() => chaneBackgroundColor('#0e9aa7')}></div>
                                        <div className="color" style={{ backgroundColor: '#3da4ab' }} onClick={() => chaneBackgroundColor('#3da4ab')}></div>
                                        <div className="color" style={{ backgroundColor: '#f6cd61' }} onClick={() => chaneBackgroundColor('#f6cd61')}></div>
                                        <div className="color" style={{ backgroundColor: '#fe8a71' }} onClick={() => chaneBackgroundColor('#fe8a71')}></div>
                                        <div className="color" style={{ backgroundColor: '#fe9c8f' }} onClick={() => chaneBackgroundColor('#fe9c8f')}></div>
                                        <div className="color" style={{ backgroundColor: '#feb2a8' }} onClick={() => chaneBackgroundColor('#feb2a8')}></div>
                                        <div className="color" style={{ backgroundColor: '#fec8c1' }} onClick={() => chaneBackgroundColor('#fec8c1')}></div>
                                        <div className="color" style={{ backgroundColor: '#fad9c1' }} onClick={() => chaneBackgroundColor('#fad9c1')}></div>
                                        <div className="color" style={{ backgroundColor: '#f9caa7' }} onClick={() => chaneBackgroundColor('#f9caa7')}></div>
                                    </div>
                                </div> :
                                        active[7] === "active" ?
                                            downloadVideos.map(data => {
                                                if (data.status === "completed") {
                                                    return <div className="download-button-container">
                                                        <a className="download-button" href={data.download} >Download </a>
                                                    </div>
                                                }
                                            }) :
                                            <div></div>
                        }
                    </div>

                    {/* End of the Feature container */}
                    {/* Start of the Edit container */}

                    <div className="canvas-container">
                        <div className="edit-container">
                            <canvas id="canvas" width={props.location.state.width} height={props.location.state.height}></canvas>
                            <canvas id="canvas1" width={props.location.state.width} height={props.location.state.height} style={{ display: "none" }} />
                        </div>

                        <section className="seens" style={{ position: 'absolute', bottom: 20, width: '120vh', overflow: 'scroll' }}>
                            {
                                arrayOfSeen.length >= 1 ? arrayOfSeen.map((data, i) => {
                                    return (
                                        <div style={{ display: "flex", flexDirection: "column", alignItems: "center", cursor: "pointer" }} onClick={() => showSeen(data)}>
                                            <h5>duration:{data.duration}</h5>
                                            <img src={data.image} alt="seen" style={{ height: props.location.state.height / 5, width: props.location.state.width / 5, marginLeft: 10 }} />
                                        </div>
                                    )
                                }) : null
                            }
                        </section>

                    </div>

                    <div>
                        <div className="edit-functionality">
                            <input type="number" placeholder="animation speed" onChange={(e) => changeTheSpeed(e)} />
                            <input type="range" min="0" max="10" className="slider" id="myRange" onChange={(e) => setTheOpacity(e)} />
                            <input type="number" value={duration} placeholder="set time in milli seconds" onChange={(e) => setduration(e.target.value)}></input>
                            <button onClick={nextSeen}>Add seen</button>
                            <button onClick={saveToExistingSeens}>Save</button>
                            <button onClick={toggleModalFunction}>Preview</button>
                            <button onClick={saveTemplate}>Template Save</button>
                            <button onClick={deleteElement}>Delete</button>
                            <button onClick={deleteSeen}>Delete seen</button>
                            <button onClick={duplicateTheElement}>Duplicate</button>
                            <button onClick={copyTheElement}>Copy</button>
                            <button onClick={pastTheElement}>Paste</button>
                            <button>Lock</button>
                            <button onClick={sendImagesToBackend}>Generate Video</button>
                            <button onClick={sendBack}>Send Back</button>
                            <button onClick={sendFront}>Send Front</button>
                            <button onClick={sendToBack}>Send To Back</button>
                            <button onClick={sendToFront}>Send To Front</button>
                        </div>
                    </div>
                </section>

                <section className="invisible">
                    <img id="svg1" src={facebook} alt="facebook-logo" style={{ display: 'none' }}></img>
                    <canvas id="mayur" style={{ display: 'none' }}></canvas>
                </section>
            </div>
        </div>
    )
}

export default EditScreen
