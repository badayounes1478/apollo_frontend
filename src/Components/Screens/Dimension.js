import React from 'react'
import '../ScreensCss/Dimension.css'

const Dimension = (props) => {


    const setTheTemplate = (index) => {
        if (index === 1) {
            props.history.push(`${props.match.params.id}/editor/`, { width: 360, height: 360 })
        } else if (index === 2) {
            props.history.push(`${props.match.params.id}/editor/`, { width: 640, height: 360 })
        } else if (index === 3) {
            props.history.push(`${props.match.params.id}/editor/`, { width: 270, height: 480 })
        } else {
            props.history.push(`${props.match.params.id}/editor/`, { width: 360, height: 450 })
        }
    }

    console.log(props)

    return (
        <div className="dimension-container">
            <h2>Select The Template</h2>
            <div className="dimension" >
                <div className="template1" onClick={() => setTheTemplate(1)}>
                    1:1
            </div>
                <div className="template2" onClick={() => setTheTemplate(2)}>
                    16:9
            </div>
                <div className="template3" onClick={() => setTheTemplate(3)}>
                    9:16
            </div>
                <div className="template4" onClick={() => setTheTemplate(4)}>
                    4:5
            </div>
            </div>
        </div>

    )
}

export default Dimension
